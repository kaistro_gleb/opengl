#pragma once

//std
#include <memory>
#include <string>
#include <math.h>
#include <map>
#include <vector>
#include <algorithm>

// utilities
#include "Log.h"


// third party
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/matrix_decompose.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "glm/gtc/type_ptr.hpp"



// typedefs
typedef unsigned int BravoObjectHandle;